import pa.settings.ExtThree

def call(script) {
    def cls_Three = new ExtThree();
    cls_Three.changePar1("TEXT");
    println cls_Three.par1;
    cls_Three.method();
}