def st2() {
    stage("hello2") {
        script {
            echo "HELLO IN VAR2"
            return "THIS IS RETURN "
        }
    }
}

def st() {
    stage("hello") {
        script {
            hello_call_params(this)
        }
    }


}

def call() {
    st()
    def a = st2()

    Integer i = 1;
    i += 1;
    println "$i"

    println a;
    return "asd";
}