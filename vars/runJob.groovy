
def call(script, jobName, params) {
    def job

    try {
        job = build job: jobName, parameters: params
    } catch(err) {
        return false
    }

    return job.getBuildVariables()
}
