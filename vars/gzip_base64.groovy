import java.io.ObjectOutputStream;
import java.util.zip.GZIPOutputStream;

def call(text) {
    def targetStream = new ObjectOutputStream()
	def zipStream = new GZIPOutputStream(targetStream)
	zipStream.write(text.getBytes('UTF-8'))
	zipStream.close()
	def zippedBytes = targetStream.toByteArray()
	targetStream.close()
	return zippedBytes.encodeBase64().toString()
}
