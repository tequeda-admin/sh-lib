def call(script) {
    echo "Begin"

    def basic = new pa.settings.Basic(script:this)
    def result = basic.run()

    if(result) {
        currentBuild.result = 'SUCCESS' //FAILURE to fail
    }

    return this
}
