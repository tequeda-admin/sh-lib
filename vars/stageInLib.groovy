def call(script) {
    checkout([
        $class: 'GitSCM', 
        branches: [[name: "master"]],
        userRemoteConfigs: [[url: "https://tequeda-admin@bitbucket.org/tequeda-admin/test.git"]]
    ])

    def result = sh "ls"
    echo "$result"

    def inClass = new pa.settings.InClass(script:this)
    result = inClass.run0()
    echo "$result"

    result = inClass.run1()
    echo "$result"

    return this
}
