import java.text.SimpleDateFormat

def call(date_format) {
    SimpleDateFormat formatter = new SimpleDateFormat(date_format)
    Date date = new Date()

    return formatter.format(date)
}
