import pa.settings.MyClassLog

def call(script) {
    script.Log.message("HELLO WORLD")
    script.Log.debug("DEBUG MESSAGE")

    MyClassLog cl = new MyClassLog();
    cl.init(script)
    cl.run();
}