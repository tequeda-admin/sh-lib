def call(String string, Map map) {
    for (def data : map) {
        println "\${" + data.getKey() + "}"
        string = string.replace("\${" + data.getKey() + "}", data.getValue().toString())
    }
    return string
}
