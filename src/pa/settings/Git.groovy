package pa.settings

class Git {
    Script script    

    Git(script) {
        this.script = script
    }

    def gitClone() {
        script.sh(returnStdout: false, script: "git clone https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib2.git")
    }

    def clear() {
        script.sh("rm -rf sh-lib2")
    }
}