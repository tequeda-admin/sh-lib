package pa.settings

class ClassForDate {
    Script script
    public ClassForDate(script) {
        this.script = script
    }

    def getDate() {
        return script.getCurrentDateTime("dd-MM-yyyy_HH-mm-ss")
    }
}