package pa.settings

import pa.settings.Func as Func

class FuncInClass{
    def func

    def init() {
        func = new Func()
    }

    def hey() {
        return func.helloWorld()
    }
}