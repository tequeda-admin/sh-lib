package pa.settings

class ClassClos {
    String msg = "Hello world";

    def getStr = { ->
        String text = "${msg}";
        text += genStrFromLst();
        
        text
    }

    def genStrFromLst() {
        List lst = ["Hello", " ", "world"]
        return lst.join("")
    }
};