package pa.settings

class Phases {
    Object run;

    Phases(script) {
        run = script;
    }

    def stage_1() {
        run.echo("HELLO")
    }

    def stage_2() {
        run.echo("WORLD")
    }
}