package pa.settings
import pa.settings.Configs as Configs

class TestImport {
    public Configs configs

    public TestImport() {
        configs = new Configs()
    }

    public String getMvnUrl() {
        configs.initMvn()
                
        return configs.mvn.url
    }
}