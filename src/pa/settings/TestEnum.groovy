package pa.settings

public enum TestEnum {    
    ALL  ("ALL"),
    MAIL ("MAIL");

    public String note;

    private TestEnum(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public static TestEnum getObjectFromString(String name) {
        for(TestEnum testEnum: values()) {
            if(testEnum.getNote().equals(name)) {
                return testEnum
            }
        }

        throw new IllegalArgumentException("No enum: [" + name + "]");
    }
}