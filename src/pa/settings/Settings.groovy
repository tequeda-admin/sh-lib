package pa.settings

class Settings implements Serializable {
    def steps

    Settings(steps) {this.steps}

    def message() {
        steps.sh "echo 'Hello world'"
    }
}