package pa.settings

class InClass{
    Script script

    public int run0() {
        script.checkout([
            $class: 'GitSCM', 
            branches: [[name: "master"]],
            userRemoteConfigs: [[url: "https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git"]]
        ])

        return 0
    }

    public String run1() {
        def result = script.sh("ls")

        return result
    }
}