package pa.settings

class Configs {
    public Git git
    public Maven mvn

    public Configs() {
        git = new Git()
        mvn = new Maven()

        hey_env();
    }

    def hey_env() {
        env.PARAM1 = "HELLO";
        env.PARAM2 = "WORDL";
    }

    static class Git {
        def url = "None"
        def branch = "None"
    }

    static class Maven {
        def url = "None"
    }
    

    def initGit() {
        git.url = "https://git.com/"
        git.branch = "master"
    }

    def initMvn() {
        mvn.url = "https://mvn.com/"
    }
}