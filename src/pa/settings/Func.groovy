package pa.settings

def helloWorld() {
    return "Hello world"
}

def sh_hello() {
    sh "echo 'Hello func sh'"
}

def sh_run() {
    res = sh("ls")
    return res
}
