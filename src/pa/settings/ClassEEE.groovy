package pa.settings

class ClassEEE {
    Object script;
    Boolean nnn = false;

    String path_for_file = "";
/*
    def ClassEEE(script) {
        this.script = script;
    }
*/
    def ClassEEE(script, path_for_file) {
        this.script = script;
        this.path_for_file = path_for_file;
    }

    def changeNode(nnn=true) {
        this.nnn = nnn;
    }

    def _hello() {
        script.echo "NODE_NAME = ${script.env.NODE_NAME}"
        script.evaluate("println('HELLO')")
    }

    def hello() {
        if(nnn) {
            script.node("built-in") {
                return _hello()
            }
        } 

        return _hello()
    }

    def createFile(file_name) {
        if(!fileExists(path_for_file)) {
            script.sh("mkdir -p $path_for_file");
        }

        script.sh("touch ${path_for_file}/${file_name}")
    }

    @Override
    @NonCPS
    protected void finalize() throws Throwable {
        try {
            println("ОЧИСТКА!!!");
            if(!path_for_file || path_for_file.trim.isEmpty() || path_for_file.trim() == "/"  || path_for_file.trim() == "null"){
                return;
            }
            
            String str_pwd = script.sh("echo \$PWD");
            println str_pwd

            println "rm -rf ${path_for_file}";
            //script.sh("rm -rf ${path_for_file}")
        } finally {
            super.finalize();
        }
    }
};