package pa.settings

import pa.settings.Func as Func

class MyClassSh{
    Script script

    def init(script) {
        this.script = script
    }

    def run() {
        script.sh "echo 'Hello class sh'"
        return "OK"
    }

    def run1() {
        def f = new Func()
        f.sh_run()
    }
}