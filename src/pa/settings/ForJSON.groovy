package pa.settings

import groovy.json.JsonSlurper

class ForJSON {
    Script script    

    ForJSON(script) {
        this.script = script
    }

    def getDataJSon() {
        String txt = '{"first": "second", "lang": "ita"}'
        def object = readJSON text: txt

        return object
    }
}