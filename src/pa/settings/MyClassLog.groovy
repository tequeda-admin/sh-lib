package pa.settings

class MyClassLog{
    Script script

    def init(script) {
        this.script = script
    }

    def run() {
        script.Log.message("echo 'Hello class sh'")
        return "OK"
    }
}