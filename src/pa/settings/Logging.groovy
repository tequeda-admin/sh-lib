package pa.settings

class Logging implements Serializable {
    private final script;

    
/*
    def Logging() {}
*/
    def Logging(script) {
        this.script = script;
    }

    def init(script) {
        this.script = script;
        if(!script.env?.DEBUG) {
            script.env.DEBUG = "false";
        }
    } 

    def message(message) {
        message = ("=" * 80) + "\n" + message + "\n" + ("=" * 80)

        script.println(message)
    }

    def info(message) {
        script.println(message)
    }

    def debug(message) {
        if(script.env.DEBUG == "true" || 
           (script.params?.DEBUG && (script.params.DEBUG == "true"))
        ) {
            script.println(message);
        }
    }
}