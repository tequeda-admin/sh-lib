package pa.settings

class Stage41 {
    Object script;

    def Stage41(script) {
        this.script = script;
    }

    def hello() {
        script.echo("Hello world");
    }

    def world() {
        script.echo("World hello");
    }
}