package pa.settings

class EnvInClass{
    Script script

    def last_log

    EnvInClass(script) {
        this.script = script
    }
    void init(lib_pa) {
        script.env.LAST_LOG_NAME = "file_name"
    }

    void func1() {
        last_log = "this last log"
    }



}