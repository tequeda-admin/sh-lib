package pa.settings

class MyClassPar{
    Script script;

    protected String param = "Hello";

    MyClassPar(script) {
        this.script = script;
    }

    def run0() {
        return "Woof"
    }

    def run0(voice) {
        return voice + "!!!"
    }

    def run1() {
        script.sh("echo 'Hello world'")
        return "OK"
    }

    void clear() {
    }
}