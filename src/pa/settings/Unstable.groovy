package pa.settings;

class Unstable extends Exception {
    public Unstable() {
        super()
    }

    public Unstable(String errorMessage) {
        super(errorMessage)
    }

    public Unstable(String errorMessage, Throwable err) {
        super(errorMessage, err)
    }
};