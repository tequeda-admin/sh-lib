package pa.settings

class MyClassDef{
    def par1
    def par2

    def run0() {
        return "Woof"
    }

    def sav1(voice) {
        par1 = voice
        par2 = voice + voice
    }

    void clear() {
    }
}